# desublimate
desublimate: Create a configuration file from env variables

# Why

Why indeed. This little script was inspired by Gitlab CI and outside of CI/CD, there might not be too much use for this.
In CI/CD pipelines it is sometimes useful to generate configuration files depending on the target environment and sometimes
using tools such as Ansible or Terraform can be overkill if the intention is to just generate simple files. Since most of the
information is relayed via environment variables, templating and/or automatic creation of the said configuration files is a 
tempting option.

This little script does just that. In CI environment one can define variables for example `PRODUCTION_DOMAIN` or `STAGING_DOMAIN`
and this script does the rest so that just the `DOMAIN` part ends up in the configuration file. At simplest the files are
plain JSON or YAML, but can be more complex. For that use case this tool supports [Handlebars](https://handlebarsjs.com/) templates.

# Installation

`npm install -g desublimate` for global install or `npm install desublimate` for local.

# Usage

## Notice

Currently, by default, the tool assumes that all the files are encoded with UTF-8. Support for other character encodings is coming up!

### Command line parameters:
```
--envFile | -e:
    Description: Variable file (dotenv-like). Used for additional variables and is applied AFTER filtering with varPrefix and varSuffix. Accepts multiple files separated with a comma.

--debug | -d:
    Description: Show debug prints.

--doNotStrip | -D:
    Description: Do not strip prefix and suffix from the environment variable.
    Default value: false

--help | -h:
    Description: This command

--ignoreEnvironment | -I:
    Description: Ignore the environment variables completely for safety and to clarify that environment is being ignored.
    Default value: false

--output | -o:
    Description: Output target.
    Possible values: file, stdout, stderr
    Default value: stdout

--outputFile | -O:
    Description: Filename and path of the file when using file output. Sets '--output file' automatically.

--parseSubobject | -b:
    Description: Create multilevel objects from environment variables

--renderer | -r:
    Description: Renderer used to format the configuration.
    Possible values: dotenv, json, yaml, handlebars
    Default value: json

--silent | -s:
    Description: Suppress all log output.
    Default value: false

--subobjectSeparator | -B:
    Description: Character or characters used to separate the different object level in the environment variables.
    Default value: __

--template | -t:
    Description: Filename and path of the Handlebars template file.

--version | -v:
    Description: undefined

--varPrefix | -P:
    Description: Variable prefix used to filter the environment variables.
    Default value:

--varSuffix | -S:
    Description: Variable suffix used to filter the environment variables.
    Default value:
```

### Configuration file

Now a configuration file can be used to configure the... configuration file creating tool (I honestly did not realize how
meta this is until writing this down :D ). The configuration file is named `.e2crc` and must be placed to the working directory.
The order of evaluation for configuration is: 1. Defaults, 2. `.e2crc` 3. CLI arguments. The configuration file uses JSON
format and the options are the same as when used via CLI (e.g. `"envFile": "./.myenv"` or `"debug": true`).


### .env file

desublimate supports now dotenv-like `.env` file. By this I mean that just like with dotenv, the file is placed to the
working directory and variables are defined `<KEY>=<VALUE>`, the formatting done to the variables, however, does differentiate
from dotenv. This means that while comments (lines starting with #) are supported, features like quote conversion and multiline
are missing. While it is not guaranteed that at some point down the line the tool would have full dotenv file support, it is
also not guaranteed that it will happen, only time will tell. 

By default, desublimate will look for `.env` file, but this can be overridden either in the configuration file or on command line.

Following examples assume that the package is installed globally.

Example using handlebars template:

`BAR_SOMETHING_ELSE_FOO=000 BAR_SOMETHING_FOO=123 BAR_SOME=456 SOME_FOO=789 desublimate --renderer handlebars --varSuffix _FOO --varPrefix BAR_ --template test.hb --output file --outputFile ../foobar`

Example with default json output and stdout:

`BAR_SOMETHING_ELSE_FOO=000 BAR_SOMETHING_FOO=123 BAR_SOME=456 SOME_FOO=789 desublimate --varSuffix _FOO --varPrefix BAR_`

Example with multiple .env source files and dotenv output:

`desublimate -I --envFile first.env,second.env,third.env --renderer dotenv`

### Evaluation order

The evaluation order is:
1. .env file
   1. If using multiple source .env files, then those are evaluated in the order they are given.
2. environment variables

This way the file can be used to set the default values which then can be overridden with environment variables.

## Notice

In the configuration file when not using a handlebars template, the prefix and suffix are stripped from the variable name.
