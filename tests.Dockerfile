FROM node:12-slim

RUN apt-get update && apt-get install -y python3-pip
RUN npm install -g bats
RUN pip3 install pyyaml

WORKDIR /desublimate/tests

ENTRYPOINT ["./run-tests.sh"]
