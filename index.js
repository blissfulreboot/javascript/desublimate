#!/usr/bin/env node

const { Generate } = require("./src/generator");
const { Logger } = require("./src/logger");
const { outputHelp, outputVersion, parseArguments } = require("./src/commandline");
if (!module.parent) {
  let opts = parseArguments();

  Logger.setSilent(opts.silent);

  if (opts.debug) Logger.debug("Raw command line arguments: ", JSON.stringify(opts, null, 2));

  if (opts.help) {
    outputHelp();
    process.exit(0);
  }

  if (opts.version) {
    outputVersion();
    process.exit(0);
  }

  if (opts.debug) Logger.debug("Options: ", JSON.stringify(opts, null, 2));

  Generate(opts);
}
