# [2.2.0](https://gitlab.com/blissfulreboot/javascript/desublimate/compare/v2.1.3...v2.2.0) (2022-08-30)


### Features

* support for multiple dotenv files and dotenv format output ([7875d11](https://gitlab.com/blissfulreboot/javascript/desublimate/commit/7875d119cff9941490878ec093544e61a6ca0cde))

## [2.1.3](https://gitlab.com/blissfulreboot/javascript/desublimate/compare/v2.1.2...v2.1.3) (2021-04-06)


### Bug Fixes

* update dependencies ([d01dfb8](https://gitlab.com/blissfulreboot/javascript/desublimate/commit/d01dfb8a7c9fcb0091a8a8dcc0a27a638ba968cf))

## [2.1.2](https://gitlab.com/blissfulreboot/javascript/desublimate/compare/v2.1.1...v2.1.2) (2020-09-19)


### Bug Fixes

* update outdated dependencies ([90c5874](https://gitlab.com/blissfulreboot/javascript/desublimate/commit/90c587482cffe54fa4c3a934193a952c32e05bf6))

## [2.1.1](https://gitlab.com/blissfulreboot/javascript/desublimate/compare/v2.1.0...v2.1.1) (2020-04-07)


### Bug Fixes

* fix vulnerable dependencies ([56193e6](https://gitlab.com/blissfulreboot/javascript/desublimate/commit/56193e69d2f866a17b3929a25659bab989cf1e1d))

# [2.1.0](https://gitlab.com/blissfulreboot/javascript/desublimate/compare/v2.0.0...v2.1.0) (2020-03-14)


### Bug Fixes

* **commandline:** Fix typo in aliases ([558f8a9](https://gitlab.com/blissfulreboot/javascript/desublimate/commit/558f8a99fbeaa7f0c8e04c070e125f63bc12814b))


### Features

* **commandline:** Add short aliases for commands ([862ba00](https://gitlab.com/blissfulreboot/javascript/desublimate/commit/862ba006fc03cbbaf3290a8ec373d19312a6e42a))
* **Subobject support:** Add support for defining subobjects with env variables ([9618bfa](https://gitlab.com/blissfulreboot/javascript/desublimate/commit/9618bfa2c9c18614aa1fe06a87d6e0cb9eba416b))

# [2.0.0](https://gitlab.com/blissfulreboot/javascript/desublimate/compare/v1.5.0...v2.0.0) (2020-03-11)


### Code Refactoring

* **project name:** change the name from env2conffile to something neutral ([dc2ffa4](https://gitlab.com/blissfulreboot/javascript/desublimate/commit/dc2ffa47af04f20259958d3c03f0e64e4660eac8))


### BREAKING CHANGES

* **project name:** Name (and command) changed from env2conffile to desublimate

# [1.5.0](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.4.0...v1.5.0) (2020-03-09)


### Features

* add option to suppress any other stdout or stderr output than the generated document ([83cd318](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/83cd318ee6add15e24fff1391925ad939cc6c31d))

# [1.4.0](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.3.3...v1.4.0) (2020-03-05)


### Features

* **environment:** add option to completely ignore the environment variables ([458d6e3](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/458d6e348b8edee611e3e9bd9e318e1a993b3901)), closes [#4](https://gitlab.com/blissfulreboot/javascript/env2conffile/issues/4)

## [1.3.3](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.3.2...v1.3.3) (2020-02-29)


### Bug Fixes

* **package.json:** Update the issue page and homepage of the project ([73aa8bd](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/73aa8bd103fe231c2c53d5f1c595c7eefe945e54))

## [1.3.2](https://gitlab.com/blissfulreboot/javascript/env2conffile/compare/v1.3.1...v1.3.2) (2020-02-26)


### Bug Fixes

* **Generate:** Change the order in which the variables are added to the final env object ([2ecaaae](https://gitlab.com/blissfulreboot/javascript/env2conffile/commit/2ecaaaee5d2a187b76894086c7a783b5c0b7f944)), closes [#3](https://gitlab.com/blissfulreboot/javascript/env2conffile/issues/3)

## [1.3.1](https://github.com/XC-/env2conffile/compare/v1.3.0...v1.3.1) (2019-06-10)


### Bug Fixes

* **README:** Add corrections and adjustments to README ([ff07858](https://github.com/XC-/env2conffile/commit/ff07858))

# [1.3.0](https://github.com/XC-/env2conffile/compare/v1.2.0...v1.3.0) (2019-06-10)


### Features

* **CLI:** Support for env files. Support for configuration file. ([d399468](https://github.com/XC-/env2conffile/commit/d399468))
* **Generate:** Support for .env file ([c860bb1](https://github.com/XC-/env2conffile/commit/c860bb1))

# [1.2.0](https://github.com/XC-/env2conffile/compare/v1.1.2...v1.2.0) (2019-06-03)


### Features

* **CLI:** Imply '--output file' when defining --outputFile ([204a8fd](https://github.com/XC-/env2conffile/commit/204a8fd))

## [1.1.2](https://github.com/XC-/env2conffile/compare/v1.1.1...v1.1.2) (2019-06-02)


### Bug Fixes

* **package.json:** Fix package keywords ([b6021ca](https://github.com/XC-/env2conffile/commit/b6021ca))

## [1.1.1](https://github.com/XC-/env2conffile/compare/v1.1.0...v1.1.1) (2019-06-02)


### Bug Fixes

* **doNotStrip:** Fix momentary lapse of thought in setting the default value ([851dc1b](https://github.com/XC-/env2conffile/commit/851dc1b))

# [1.1.0](https://github.com/XC-/env2conffile/compare/v1.0.1...v1.1.0) (2019-06-02)


### Bug Fixes

* **Help:** Update help to match the latest changes ([c0ac0db](https://github.com/XC-/env2conffile/commit/c0ac0db))
* **README:** Improve README ([ac828d3](https://github.com/XC-/env2conffile/commit/ac828d3))


### Features

* **Generate:** Add option to prevent prefix and suffix stripping from the var name ([43cd082](https://github.com/XC-/env2conffile/commit/43cd082))

## [1.0.1](https://github.com/XC-/env2conffile/compare/v1.0.0...v1.0.1) (2019-06-02)


### Bug Fixes

* **installation:** Set bin script ([793f792](https://github.com/XC-/env2conffile/commit/793f792))

# 1.0.0 (2019-06-02)


### Features

* First version ([b79e1f6](https://github.com/XC-/env2conffile/commit/b79e1f6))
