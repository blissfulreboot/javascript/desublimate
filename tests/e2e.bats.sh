#!/usr/bin/env bats

load helpers/assertions
load helpers/environmentVariables

teardown() {
  rm expected.json || true
  rm expected.yml || true
  rm output.json || true
  rm output.yml || true
}

@test "Check that --silent flag suppresses the informational output" {
  export TEST_ONELINE_SILENT="Foobar"
  run desublimate --varPrefix TEST_ONELINE_ --renderer yaml --silent
  assertExitStatus 0
  EXPECTED=$(cat << EOM
SILENT: Foobar
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "Check that the informational output is not suppressed when --silent is not given" {
  export TEST_ONELINE_SILENT="Foobar"
  run desublimate --varPrefix TEST_ONELINE_ --renderer yaml
  assertExitStatus 0
  EXPECTED=$(cat << EOM
Using process environment...
SILENT: Foobar
EOM
)
  assertStringEquals "$output" "$EXPECTED"
}

@test "Check that JSON can be generated from environment variables by default" {
  simpleVariables
  run desublimate --varPrefix TEST_SIMPLE_ --silent
  echo "$output" > output.json
  assertExitStatus 0
  cat >expected.json << EOM
{
  "SECONDVAR": "2",
  "FIRSTVAR": "1"
}
EOM
  ./helpers/compare.py json
}

@test "Check that JSON can be generated from environment variables when explicitly setting the renderer" {
  simpleVariables
  run desublimate --varPrefix TEST_SIMPLE_ --renderer json --silent
  echo "$output" > output.json
  assertExitStatus 0
  cat >expected.json << EOM
{
  "SECONDVAR": "2",
  "FIRSTVAR": "1"
}
EOM
  ./helpers/compare.py json
}

@test "Check that YAML can be generated from environment variables by setting renderer to yaml" {
  simpleVariables
  run desublimate --varPrefix TEST_SIMPLE_ --renderer yaml --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat >expected.yml << EOM
SECONDVAR: "2"
FIRSTVAR: "1"
EOM
  ./helpers/compare.py yaml
}

@test "Check that suffix can be used to define which environment variables are to be used" {
  suffixVariables
  run desublimate --varSuffix _SUFFIX_TEST --renderer yaml --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat >expected.yml << EOM
FIRSTVAR: "1"
SECONDVAR: "2"
EOM
  ./helpers/compare.py yaml
}

@test "Check that both prefix and suffix can be used to define which environment variables are to be used" {
  prefixedAndSuffixedTestVariables
  run desublimate --varSuffix _SUFFIX --varPrefix PREFIX_ --renderer yaml --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat >expected.yml << EOM
FIRSTVAR: "1"
SECONDVAR: "2"
EOM
  ./helpers/compare.py yaml
}


@test "Check that Handlebars templating works with only environment variables" {
  handlebarsVariables
  run desublimate --varPrefix TEST_HANDLEBARS_ --renderer handlebars --template static/environment.test.template.hb
  assertExitStatus 0
  EXPECTED=$(printf "Using process environment...\n$(cat static/environment.template.comparison.txt)")
  assertStringEquals "$output" "$EXPECTED"
}

@test "Dotenv-like file can be used to define variables (without prefix) and only those are rendered" {
  run desublimate --varPrefix THISDOESNOTMATTER --envFile static/vars.env --renderer yaml --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat > expected.yml << EOM
FILEVAR1: FILEVARVALUE1
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
EOM
  ./helpers/compare.py yaml
}

@test "Dotenv-like file can be used to define variables (without prefix) and are rendered with environment variables" {
  simpleVariables
  run desublimate --varPrefix TEST_SIMPLE_ --envFile static/vars.env --renderer yaml --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat > expected.yml << EOM
FILEVAR1: FILEVARVALUE1
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
FIRSTVAR: "1"
SECONDVAR: "2"
EOM
  ./helpers/compare.py yaml
}

@test "Dotenv-like file can be used to define variables (without prefix) and can be overridden with environment variables" {
  export OVERRIDE_FILEVAR1=9
  export OVERRIDE_FILEVAR2="foobar"
  run desublimate --varPrefix OVERRIDE_ --envFile static/vars.env --renderer yaml --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat > expected.yml << EOM
FILEVAR1: "9"
FILEVAR2: foobar
FileVar3: FileVarValue3
filevar4: filevarvalue4
EOM
  ./helpers/compare.py yaml
}

@test "User can define a list of dotenv files with values from later files overriding those from earlier in the list" {
  run desublimate -I --envFile static/vars_pt1.env,static/vars_pt2.env --renderer yaml --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat > expected.yml << EOM
FILEVAR1: NOTWHATINFIRSTFILE
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
FOO: BAR
BAR: FOO
BAZ: FOOBAR
EOM
  ./helpers/compare.py yaml
}

@test "Output can be written to a file (variables from file and environment) (output explicit)" {
  handlebarsVariables
  run desublimate --varPrefix TEST_HANDLEBARS_ --envFile static/vars.env --renderer handlebars --template static/envfile.test.template.hb --output file --outputFile results.txt
  assertExitStatus 0
  GOT=$(cat results.txt)
  EXPECTED=$(cat static/envfile.template.comparison.txt)
  assertStringEquals "$GOT" "$EXPECTED"
}

@test "Output can be written to a file (variables from file and environment) (output implicit)" {
  handlebarsVariables
  run desublimate --varPrefix TEST_HANDLEBARS_ --envFile static/vars.env --renderer handlebars --template static/envfile.test.template.hb --outputFile results.txt
  assertExitStatus 0
  GOT=$(cat results.txt)
  EXPECTED=$(cat static/envfile.template.comparison.txt)
  assertStringEquals "$GOT" "$EXPECTED"
}

@test "--ignoreEnvironment can be used to ignore the environment variables. Test 1: additional variables in environment" {
  skip
  simpleVariables
  run desublimate --varPrefix TEST_SIMPLE_ --envFile static/vars.env --renderer yaml --ignoreEnvironment --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat > expected.yml << EOM
FILEVAR1: FILEVARVALUE1
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
EOM
  ./helpers/compare.py yaml
}

@test "--ignoreEnvironment can be used to ignore the environment variables. Test 2: variable override" {
  skip
  export OVERRIDE_FILEVAR1=9
  export OVERRIDE_FILEVAR2="foobar"
  run desublimate --varPrefix OVERRIDE_ --envFile static/vars.env --renderer yaml --ignoreEnvironment --silent
  echo "$output" > output.yml
  assertExitStatus 0
  cat > expected.yml << EOM
FILEVAR1: FILEVARVALUE1
FILEVAR2: "2"
FileVar3: FileVarValue3
filevar4: filevarvalue4
EOM
  ./helpers/compare.py yaml
}

@test "Parse object from env variables and output as json" {
  objectNotationVariables
  run desublimate -P TEST_OBJECT_ -r json -s -b
  echo "$output" > output.json
  assertExitStatus 0
  cat > expected.json << EOM
{
  "FOO": {
    "BAR": {
      "FOOBAR": "123",
      "RABOOF": "small"
    },
    "RAB": "444"
  }
}
EOM
  ./helpers/compare.py json
}

@test "Parse object from env variables and output as yaml" {
  objectNotationVariables
  run desublimate -P TEST_OBJECT_ -r yaml -s -b
  echo "$output" > output.yml
  assertExitStatus 0
  cat > expected.yml << EOM
FOO:
  BAR:
    FOOBAR: "123"
    RABOOF: small
  RAB: "444"
EOM
  ./helpers/compare.py yaml
}

@test "Fail with exit code 1 if unknown output is given (not stdout, stderr or file)" {
  run desublimate --output foobar
  assertExitStatus 1
}

@test "Fail with exit code 2 if unknown renderer is given (not json, yaml or handlebars)" {
  run desublimate --renderer foobar
  assertExitStatus 2
}

@test "Fail with exit code 3 if output type is file but no file is defined" {
  run desublimate --output file
  assertExitStatus 3
}

@test "Fail with exit code 4 if handlebars renderer is used but no template given" {
  handlebarsVariables
  run desublimate --varPrefix TEST_HANDLEBARS_ --renderer handlebars
  assertExitStatus 4
}

