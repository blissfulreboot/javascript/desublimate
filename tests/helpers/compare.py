#!/usr/bin/env python3

import sys
import yaml
import json
import pprint

file_extension = ""

if sys.argv[1] == "yaml":
    file_extension = "yml"
elif sys.argv[1] == "json":
    file_extension = "json"
else:
    print("Unsupported filetype")
    exit(1)

output = None
expected = None

try:
    with open("output." + file_extension, "r") as f:
        output = json.load(f) if file_extension == "json" else yaml.load(f, Loader=yaml.Loader)

    with open("expected." + file_extension, "r") as f:
        expected = json.load(f) if file_extension == "json" else yaml.load(f, Loader=yaml.Loader)
except Exception as e:
    print(e)
    print(file_extension)
    exit(1)

if output == expected:
    exit(0)
else:
    pp = pprint.PrettyPrinter(indent=3)
    print("Expected:")
    pp.pprint(expected)
    print("")
    print("Actual output:")
    pp.pprint(output)
    exit(1)
