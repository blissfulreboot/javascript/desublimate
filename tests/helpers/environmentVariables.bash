simpleVariables() {
  export TEST_SIMPLE_FIRSTVAR=1
  export TEST_SIMPLE_SECONDVAR=2
}

suffixVariables() {
  export FIRSTVAR_SUFFIX_TEST=1
  export SECONDVAR_SUFFIX_TEST=2
}

prefixedAndSuffixedTestVariables() {
  export PREFIX_FIRSTVAR_SUFFIX=1
  export PREFIX_SECONDVAR_SUFFIX=2
  export THIRDVAR_SUFFIX=3
  export PREFIX_FOURTHVAR=4
}

handlebarsVariables() {
  export TEST_HANDLEBARS_FIRSTVAR=1
  export TEST_HANDLEBARS_SECONDVAR=2
  export TEST_HANDLEBARS_camelCaseVar=3
  export TEST_HANDLEBARS_smallvarsmallval=small
}

objectNotationVariables() {
  export TEST_OBJECT_FOO__BAR__FOOBAR=123
  export TEST_OBJECT_FOO__BAR__RABOOF=small
  export TEST_OBJECT_FOO__RAB=444
}