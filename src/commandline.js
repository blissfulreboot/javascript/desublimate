const fs = require("fs");
const path = require("path");
const minimist = require("minimist");

const pkgJson = require(path.join( "..", "package.json"));

const DEFAULT_ENV_FILE = path.join(process.cwd(), ".env");
const CONFIGURATION_FILE = ".e2crc";

const DEFAULTS = {
  output: "stdout",
  renderer: "json",
  varPrefix: "",
  varSuffix: "",
  doNotStrip: false,
  ignoreEnvironment: false,
  silent: false,
  parseSubobjects: false,
  subobjectSeparator: "__"
};

const HELP = {
  "envFile": {
    alias: "e",
    description: "Variable file (dotenv-like). Used for additional variables and is applied AFTER filtering with varPrefix and varSuffix. Accepts multiple files separated with a comma."
  },
  "debug": {
    alias: "d",
    description: "Show debug prints."
  },
  "doNotStrip": {
    alias: "D",
    description: "Do not strip prefix and suffix from the environment variable."
  },
  "help": {
    alias: "h",
    description: "This command"
  },
  "ignoreEnvironment": {
    alias: "I",
    description: "Ignore the environment variables completely for safety and to clarify that environment is being ignored."
  },
  "output": {
    alias: "o",
    description: "Output target.",
    values: ["file", "stdout", "stderr"]
  },
  "outputFile": {
    alias: "O",
    description: "Filename and path of the file when using file output. Sets '--output file' automatically."
  },
  "parseSubobject": {
    alias: "b",
    description: "Create multilevel objects from environment variables"
  },
  "renderer": {
    alias: "r",
    description: "Renderer used to format the configuration.",
    values: ["dotenv", "json", "yaml", "handlebars"]
  },
  "silent": {
    alias: "s",
    description: "Suppress all log output."
  },
  "subobjectSeparator": {
    alias: "B",
    description: "Character or characters used to separate the different object level in the environment variables."
  },
  "template": {
    alias: "t",
    description: "Filename and path of the Handlebars template file."
  },
  "version": {
    alias: "v",
    desciption: "Print the version of this software."
  },
  "varPrefix": {
    alias: "P",
    description: "Variable prefix used to filter the environment variables."
  },
  "varSuffix": {
    alias: "S",
    description: "Variable suffix used to filter the environment variables."
  }
};

const ALIASES = {
  b: "parseSubobjects",
  B: "subobjectSeparator",
  e: "envFile",
  d: "debug",
  D: "doNotStrip",
  h: "help",
  I: "ignoreEnvironment",
  o: "output",
  O: "outputFile",
  r: "renderer",
  s: "silent",
  t: "template",
  v: "version",
  P: "varPrefix",
  S: "varSuffix",
};

function outputHelp() {
  console.log(`\n${pkgJson.name} version ${pkgJson.version} command line parameters:\n\n`);
  Object.keys(HELP).forEach((key) => {
    console.log(`--${key} | -${HELP[key].alias}:`);
    console.log(`    Description: ${HELP[key].description}`);
    if (HELP[key].values) {
      console.log(`    Possible values: ${HELP[key].values.join(", ")}`);
    }
    if (JSON.stringify(DEFAULTS[key])) {
      console.log(`    Default value: ${DEFAULTS[key]}`);
    }
    console.log("");
  })
}

function outputVersion() {
    console.log(`${pkgJson.name} ${pkgJson.version}`);
}

function parseArguments() {
  const args = process.argv.slice(2);
  let optsFromFile;
  try {
    const rawConfFromFile = fs.readFileSync(path.join(process.cwd(), CONFIGURATION_FILE));
    optsFromFile = JSON.parse(rawConfFromFile);
  } catch(e) {
    // pass
  }
  const optsFromLine = minimist(args, { alias: ALIASES });

  const opts = Object.assign({}, DEFAULTS, optsFromFile, optsFromLine);

  if (opts.outputFile) opts.output = "file";

  if (!opts.envFile) {
    if (fs.existsSync(DEFAULT_ENV_FILE)) opts.envFile = DEFAULT_ENV_FILE;
  }

  return opts
}

module.exports = {
  outputHelp,
  outputVersion,
  parseArguments
};
