function unflattenObject(separator, flattenedString, value) {
  let parts = flattenedString.split(separator);
  const createObject = (p, v) => {
    if (p.length === 1) return { [p[0]]: v };
    return { [p[0]]: createObject(p.splice(1), v) }
  };
  return createObject(parts, value);
}

function mergeObjects(objectList) {
  const mergeToResult = (o, target) => {
    return Object.keys(o).reduce((acc, i) => {
      if (i in acc) {
        mergeToResult(o[i], acc[i])
      } else {
        acc[i] = o[i];
      }
      return acc
    }, target)
  };
  return objectList.reduce((acc, item) => {
    return mergeToResult(acc, item)
  }, {})
}

module.exports = {
  unflattenObject,
  mergeObjects
};
